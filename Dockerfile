# Image used to build the docker image
FROM maven:3-jdk-8-alpine

# Command Used to create a Directory in Container
WORKDIR /usr/src/app

#Copy the root directory files in gitlab to the container Working Directory
COPY . /usr/src/app

#Maven Build
RUN mvn package

# Create an environment to expose
ENV PORT 5000

# Port Exposing
EXPOSE $PORT

#Command to run spring boot application with the exposed port
CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
